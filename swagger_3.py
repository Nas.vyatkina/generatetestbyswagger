import json
import os
import random
import sys
import traceback

from random_words import RandomWords

from generatetestbyswagger.swagger_parser import InterfeceSwaggerParser

rw = RandomWords()
# from pprint import pprint


class Swagger3Parser(InterfeceSwaggerParser):
    PATH_TO_DIR = os.getcwd()
    TITLE_MAIN_DIR = '/tests'

    @staticmethod
    def _read_json(path):
        with open(path) as data_file:
            data = json.load(data_file)
        return data

    @staticmethod
    def _stop_parse():
        print('в файле нет неободимых данных для построения тестов')

    @staticmethod
    def ensure_dir(directory):
        try:
            os.stat(directory)
        except FileNotFoundError:
            # traceback.print_exc()
            os.mkdir(directory)

    @staticmethod
    def parse_schema(data, models):
        # print(4)

        def parse_data(name):
            res = []
            for p in models[name]['properties'].keys():
                if models[name]['properties'][p].get('type', False):
                    tmp = {
                        'name': p,
                        'type': models[name]['properties'][p]['type']
                    }
                    if tmp['type'] == 'array':
                        if models[name]['properties'][p]['items'].get('$ref', False):
                            tmp['items'] = parse_data(models[name]['properties'][p]['items']['$ref'].split('/')[-1])
                        else:
                            tmp['items'] = models[name]['properties'][p]['items']

                    if models[name]['properties'][p].get('enum'):
                        tmp['enum'] = models[name]['properties'][p]['enum']
                    if models[name]['properties'][p].get('example'):
                        tmp['example'] = models[name]['properties'][p].get('example')
                    if tmp['type'] == "object":
                        #print(models)
                        #print(name)
                        #print(p)
                        if models[name]['properties'][p].get('$ref', False):
                            model_ = models[name]['properties'][p]['$ref'].split('/')[-1]
                            #print(model_)
                            tmp['properties'] = parse_data(model_)
                        elif models[name]['properties'][p].get('properties', False):
                            model_ = models[name]['properties'][p]['properties']
                            tmp['properties'] = parse_object(model_)
                else:
                    tmp = {
                        'name': p,
                        'type': parse_data(models[name]['properties'][p]['$ref'].split('/')[-1])
                    }
                res.append(tmp)
            return res

        def parse_object(model_):
            #print(model_)
            temp = []

            if model_.get("$ref", False):
                    parse_data(model_['$ref'].split('/')[-1])
            else:
                for p in model_.keys():
                    #print(p)
                    #print(model_[p])
                    a = {"name": p,
                         "type": model_[p]["type"]}
                    if model_[p]["type"] == "object":
                        #print(model_)

                        a["properties"] = parse_object(model_=model_[p]['properties'])
                    elif model_[p]["type"] == "array":
                        #print(model_[p]['items'])
                        if model_[p]['items'].get("anyOf", False):
                            for i in range(len(model_[p]['items']['anyOf'])):
                                if model_[p]['items']['anyOf'][i].get('$ref', False):
                                    parse_data(model_[p]['items']['anyOf'][i]['$ref'].split('/')[-1])
                                elif model_[p]['items']['anyOf'][i]["type"] == "object":
                                    if model_[p]['items']['anyOf'][i].get("properties", False):
                                        a['items'] = parse_object(model_=model_[p]['items']['anyOf'][i]['properties'])
                                else:
                                    a['items'] = model_[p]['items']['anyOf'][i]
                        else:
                            if model_[p]['items'].get('$ref', False):
                                parse_data(model_[p]['items']['$ref'].split('/')[-1])
                            elif model_[p]['items']["type"] == "object":
                                if model_[p]['items'].get("properties", False):
                                    a['items'] = parse_object(model_=model_[p]['items']['properties'])
                            else:
                                a['items'] = model_[p]['items']
                    temp.append(a)
            return temp

        if data.get('$ref', False):
            return parse_data(data['$ref'].split('/')[-1])

        else:
            return {}

    def __init__(self, dict_swagger=None, path_to_swagger=None):

        if not dict_swagger and not path_to_swagger:
            print('нет необходимых аргументов для выполнения запроса')
            sys.exit(0)
        if dict_swagger:
            super(Swagger3Parser, self).__init__(dict_swagger)
        else:
            try:
                super(Swagger3Parser, self).__init__(Swagger3Parser._read_json(dict_swagger))
            except FileNotFoundError:
                print('не вышло прочитать файл')
                sys.exit(0)
        self.base_url = ''
        self.parse()

    def parse(self):
        try:
            self.base_url = self.dict_swagger['servers'][0]['url']
            print(self.base_url)
        except KeyError:
            #print(self.dict_swagger)
            print(traceback.print_exc())
            Swagger3Parser._stop_parse()
        self.__generate_structures_by_tags()


    def __generate_structures_by_tags(self):

        def generate_response_code_data(result, data_json, key):
            #print(key)
            for i in key:
                #print(result)
                #print(i)
                if result[i]["type"] == "array":
                    data_json[result[i]["name"]] = {"type": "list", "next": []}
                    if result[i].get("items", False):
                        if type(result[i]["items"]) == list:
                            data_json[result[i]["name"]]["next"].append({"type": "dict", "next": {}})
                            generate_response_code_data(result=result[i]["items"],
                                                        data_json=data_json[result[i]["name"]]["next"][0]["next"],
                                                        key=range(len(result[i]["items"])))
                        elif result[i]["items"].get("anyOf", False):
                            data_json[result[i]["name"]]["next"].append(
                                {"type": "list", "next": []})
                            generate_response_code_data(result=result[i]["items"]["anyOf"],
                                                        data_json=data_json[result[i]["name"]]["next"][0]["next"],
                                                        key=range(len(result[i]["items"])))
                        elif result[i]["items"].get("items", False):



                            data_json[result[i]["name"]]["next"].append({"type": "list", "next": []})
                            #print(result)
                            if result[i]["items"]["items"].get("anyOf", False):
                                generate_response_code_data(result=result[i]["items"]["items"]["anyOf"],
                                                            data_json=data_json[result[i]["name"]]["next"][0]["next"],
                                                            key=range(len(result[i]["items"]["items"]["anyOf"])))
                            else:
                                generate_response_code_data(result=[result[i]["items"]["items"]],
                                                            data_json=data_json[result[i]["name"]]["next"][0]["next"],
                                                            key=range(len(result[i]["items"]["items"])))


                        else:
                            data_json[result[i]["name"]]["next"].append({"type": result[i]["items"]["type"], "next": None})
                elif result[i]["type"] == "object":
                    if result[i].get('properties', False):
                        data_json[result[i]["name"]] = {"type": "dict", "next": {}}
                        generate_response_code_data(result=result[i]["properties"],
                                                    data_json=data_json[result[i]["name"]]["next"],
                                                    key=range(len(result[i]["properties"])))
                    else:
                        data_json[result[i]["name"]] = {"type": "dict", "next": None}
                else:
                    if result[i].get("name", False):
                        data_json[result[i]["name"]] = {"type": result[i]["type"], "next": None}
                    else:
                        #print(data_json)
                        data_json.append({"type": result[i]["type"], "next": None})

        def test_value_random_string(result, value):
            a = 1
            if result.get("minLength", False) and result.get("maxLength", False):
                if result["minLength"] < len(value) < result["maxLength"]:
                    a = 0
            elif result.get("minLength", False):
                if len(value) > result["minLength"]:
                    a = 0
            elif result.get("maxLength", False):
                if len(value) < result["maxLength"]:
                    a = 0
            else:
                a = 0
            return a

        def test_value_random_integer(result, value):
            a = 1
            if result.get("minimum", False) and result.get("maximum", False):
                if result["minimum"] < value < result["maximum"]:
                    a = 0
            elif result.get("minimum", False):
                if value > result["minimum"]:
                    a = 0
            elif result.get("maximum", False):
                if value < result["maximum"]:
                    a = 0
            else:
                a = 0
            return a

        def generate_json_data_successful(result, summary):
            data = dict(url=self.base_url + result["url"], method=result["method"], body={}, headers={},
                        query={}, responses={}, summary=summary, struct_microtest={})

            if result["header"]:
                value = ""
                for arr in range(len(result["header"])):
                    key_ = result["header"][arr]["name"]
                    value = result["header"][arr]["value"]
                    if type(result["header"][arr]["value"]) == list:
                        value = random.choice(result["header"][arr]["value"])
                    if not value:
                        if result["header"][arr]["type"] == "string":
                            a = 1
                            while a == 1:
                                value = rw.random_word()
                                a = test_value_random_string(result["header"][arr], value)
                        elif result["header"][arr]["type"] == "integer":
                            a = 1
                            while a == 1:
                                value = random.randint(1, 500)
                                a = test_value_random_integer(result["header"][arr], value)

                    data["headers"][key_] = value

            if result["body"]:
                value = ""
                for arr in range(len(result["body"])):
                    for row in range(len(result["body"][arr]["data"])):
                        key_ = result["body"][arr]["data"][row]["name"]
                        if result["body"][arr]["data"][row].get("enum", False):
                            value = random.choice(result["body"][arr]["data"][row]["enum"])
                        else:
                            if result["body"][arr]["data"][row]["type"] == "string":
                                a = 1
                                while a == 1:
                                    value = rw.random_word()
                                    a = test_value_random_string(result["body"][arr]["data"][row], value)
                            elif result["body"][arr]["data"][row]["type"] == "integer":
                                a = 1
                                while a == 1:
                                    value = random.randint(1, 500)
                                    a = test_value_random_integer(result["body"][arr]["data"][row], value)
                            elif result["body"][arr]["data"][row]["type"] == "boolean":
                                value = random.choice([True, False])
                            elif result["body"][arr]["data"][row]["type"] == "array":
                                if type(result["body"][arr]["data"][row]["items"]) == dict:
                                    if result["body"][arr]["data"][row]["items"]["type"] == "integer":
                                        value = [random.randint(1, 500)]
                                    elif result["body"][arr]["data"][row]["items"]["type"] == "string":
                                        value = [rw.random_word()]

                        data["body"][key_] = value

            if result["query"]:
                value = ""
                for arr in range(len(result["query"])):
                    key_ = result["query"][arr]["name"]
                    if type(result["query"][arr]["value"]) == list:
                        value = random.choice(result["query"][arr]["value"])
                    else:
                        if result["query"][arr]["type"] == "string":
                            a = 1
                            while a == 1:
                                value = rw.random_word()
                                a = test_value_random_string(result["query"][arr], value)
                        elif result["query"][arr]["type"] == "integer":
                            a = 1
                            while a == 1:
                                value = random.randint(1, 500)
                                a = test_value_random_integer(result["query"][arr], value)
                    data["query"][key_] = value

            if result["path"]:
                path_name = {}
                for row in range(len(result['path'])):
                    if type(result["path"][row]["value"]) == list:
                        path_name[result["path"][row]["name"]] = random.choice(result["path"][row]["value"])
                    else:
                        if result["path"][row]["value"]:
                            path_name[result["path"][row]["name"]] = result["path"][row]["value"]
                        else:
                            if result["path"][row]["type"] == "integer":
                                a = 1
                                while a == 1:
                                    path_name[result["path"][row]["name"]] = random.randint(1, 500)
                                    a = test_value_random_integer(result["path"][row], path_name[result["path"][row]["name"]])
                            else:
                                a = 1
                                while a == 1:
                                    path_name[result["path"][row]["name"]] = rw.random_word()
                                    a = test_value_random_string(result["path"][row], path_name[result["path"][row]["name"]])

                data['url'] = data['url'].format(**path_name)

            data_json = {}
            if result["responses"].get("200", False):
                if result["responses"]["200"].get("schema", False):
                    #print(result["responses"]["200"]["schema"])
                    generate_response_code_data(result["responses"]["200"]["schema"],
                                                data_json,
                                                key=range(len(result["responses"]["200"]["schema"])))
            data['dict_check_for_output'] = data_json

            data["responses"][200] = {"action": None}
            data["struct_microtest"] = {"name": "Успешно"}
            return data

        def generate_json_data_unsuccessful(result, summary):
            data = {"url": self.base_url + result["url"], "method": result["method"], "responses": {},
                    "data": [], "summary": summary}
            for i in result["responses"].keys():
                if i != "200":
                    data["responses"][i] = {"action": None}
            for num_resp in data["responses"].keys():

                dump = {"headers": {}, "body": {}, "query": {}, "struct_microtest": {}}
                if result["header"]:
                    value = ""
                    for arr in range(len(result["header"])):
                        key_ = result["header"][arr]["name"]
                        value = result["header"][arr]["value"]
                        if type(result["header"][arr]["value"]) == list:
                            value = random.choice(result["header"][arr]["value"])
                        if not value:
                            if result["header"][arr]["type"] == "string":
                                a = 1
                                while a == 1:
                                    value = rw.random_word()
                                    a = test_value_random_string(result["header"][arr], value)
                            elif result["header"][arr]["type"] == "integer":
                                a = 1
                                while a == 1:
                                    value = random.randint(1, 500)
                                    a = test_value_random_integer(result["header"][arr], value)
                        dump["headers"][key_] = value

                if result["body"]:
                    value = ""
                    for arr in range(len(result["body"])):
                        for row in range(len(result["body"][arr]["data"])):
                            key_ = result["body"][arr]["data"][row]["name"]
                            if result["body"][arr]["data"][row].get("enum", False):
                                value = random.choice(result["body"][arr]["data"][row]["enum"])
                            else:
                                if result["body"][arr]["data"][row]["type"] == "string":
                                    a = 1
                                    while a == 1:
                                        value = rw.random_word()
                                        a = test_value_random_string(result["body"][arr]["data"][row], value)
                                elif result["body"][arr]["data"][row]["type"] == "integer":
                                    a = 1
                                    while a == 1:
                                        value = random.randint(1, 500)
                                        a = test_value_random_integer(result["body"][arr]["data"][row], value)
                                elif result["body"][arr]["data"][row]["type"] == "boolean":
                                    value = random.choice([True, False])
                                elif result["body"][arr]["data"][row]["type"] == "array":
                                    if type(result["body"][arr]["data"][row]["items"]) == dict:
                                        if result["body"][arr]["data"][row]["items"]["type"] == "integer":
                                            value = [random.randint(1, 500)]
                                        elif result["body"][arr]["data"][row]["items"]["type"] == "string":
                                            value = [rw.random_word()]

                            dump["body"][key_] = value

                if result["query"]:
                    value = ""
                    for arr in range(len(result["query"])):
                        key_ = result["query"][arr]["name"]
                        if result["query"][arr]["type"] == "string":
                            a = 1
                            while a == 1:
                                value = rw.random_word()
                                a = test_value_random_string(result["query"][arr], value)
                        elif result["query"][arr]["type"] == "integer":
                            a = 1
                            while a == 1:
                                value = random.randint(1, 500)
                                a = test_value_random_integer(result["query"][arr], value)
                        dump["query"][key_] = value
                name_test = result["responses"][str(num_resp)]["description"]
                # name_test = name_test[name_test.find(" ")+1:]
                dump["struct_microtest"] = {"name": name_test}

                data["data"].append(dump)

            if result["path"]:
                path_name = {}
                for row in range(len(result['path'])):
                    if result["path"][row]["value"]:
                        path_name[result["path"][row]["name"]] = result["path"][row]["value"]
                    else:
                        if result["path"][row]["type"] == "integer":
                            a = 1
                            while a == 1:
                                path_name[result["path"][row]["name"]] = random.randint(1, 500)
                                a = test_value_random_integer(result["path"][row], path_name[result["path"][row]["name"]])
                        else:
                            a = 1
                            while a == 1:
                                path_name[result["path"][row]["name"]] = rw.random_word()
                                a = test_value_random_string(result["path"][row], path_name[result["path"][row]["name"]])

                data['url'] = data['url'].format(**path_name)

            data_json = {}
            #if result["responses"]["200"].get("schema", False):
            #    generate_response_code_data(result["responses"]["200"]["schema"], data_json)
            data['dict_check_for_output'] = data_json

            return data

        def get_data_for_body(data,
                              models_):
            res = []
            if data['parameters']:
                for d in data['parameters']:
                    if d['in'] == 'body':
                        tmp = {
                            'is_required': d.get('required'),
                            'data': Swagger3Parser.parse_schema(d['schema'],
                                                                models_)
                        }
                        res.append(tmp)
            return res

        def get_data_for_(data,
                          type_):
            # print(1)
            res = []
            #print(data)
            if data['parameters']:
                for d in data['parameters']:
                    if d['in'] == type_:
                        if d['schema'].get('enum', False):
                            value = d['schema']['enum']
                        else:
                            value = ""
                        res.append({
                            'name': d['name'],
                            'is_required': d.get('required', False),
                            'type': d['schema'].get('type', 'string'),
                            'pattern': d.get('pattern', None),
                            'value': value
                        })
            if type_ == 'header':
                if data.get('consumes', False):
                    res.append({
                        'name': 'accept',
                        'value': data['consumes'][0],
                        'type': 'string'
                    })
                if data.get('produces', False):
                    res.append({
                        'name': 'Content-Type',
                        'value': data['produces'][0],
                        'type': 'string'
                    })
            return res

        def get_data_responses(data,
                               model_):
            answer = {}
            for p_ in data.keys():
                tmp = {
                    'description': data[p_]['description'],
                }
                #print(data[p_])
                if data[p_].get("content", False):
                    if data[p_]["content"]['application/json'].get('schema', False):
                        tmp['schema'] = Swagger3Parser.parse_schema(data[p_]["content"]['application/json']['schema'],
                                                                model_)
                answer[p_] = tmp
            return answer

        def parse_tags(self_):
            self_.list_tags = [tag_['name'] for tag_ in self_.dict_swagger['tags']]

        parse_tags(self)
        Swagger3Parser.ensure_dir('{}/{}'.format(self.PATH_TO_DIR,self.TITLE_MAIN_DIR))
        for tag in self.list_tags:
            Swagger3Parser.ensure_dir('{}/{}/{}'.format(self.PATH_TO_DIR,
                                                        self.TITLE_MAIN_DIR,
                                                        tag))
        paths = self.dict_swagger['paths']
        models = self.dict_swagger['components']['schemas']
        ans = []
        open('directories.json', 'w').close()
        #open('name_methods.txt', 'w').close()

        tags = {}
        for tag_n in range(len(self.dict_swagger['tags'])):
        #    with open("directories.txt", "a") as file:
        #        file.write(self.dict_swagger['tags'][tag_n]["name"] + '\n')
            tags[self.dict_swagger['tags'][tag_n]["name"]] = []

        for p in paths.keys():
            for method in paths[p].keys():
                #print(paths[p][method])
                result_ = {
                    'url': p,
                    'method': method,
                    'tags': paths[p][method]['tags'],
                    'operationId': paths[p][method]['operationId'],
                    'header': get_data_for_(paths[p][method],
                                            'header'),
                    'body': get_data_for_body(paths[p][method],
                                              models),
                    'query': get_data_for_(paths[p][method],
                                           'query'),
                    'path': get_data_for_(paths[p][method],
                                          'path'),
                    'file': get_data_for_(paths[p][method],
                                          'file'),
                    'responses': get_data_responses(paths[p][method]['responses'],
                                                    models)
                }

                for r in result_['tags']:
                    with open(self.PATH_TO_DIR
                                      + '/'
                                      + self.TITLE_MAIN_DIR
                                      + '/'
                                      + r
                                      + '/'
                                      + result_['operationId']
                                      + '.json', 'w') as file:
                        # file.write(result_)
                        json.dump(result_, file)
                    #with open("name_methods.txt", "a") as file:
                    #    file.write(result_['operationId'] + '\n')
                    #print(tags)
                    tags[r].append(result_['operationId'])
                    json_data_successful = generate_json_data_successful(result=result_,
                                                                         summary = paths[p][method]['summary'])
                    with open(self.PATH_TO_DIR
                                      + '/'
                                      + self.TITLE_MAIN_DIR
                                      + '/'
                                      + r
                                      + '/'
                                      + result_['operationId']
                                      + "_data_successful"
                                      + '.json', "w") as file:
                        json.dump(json_data_successful, file)

                    json_data_unsuccessful = generate_json_data_unsuccessful(result=result_,
                                                                             summary=paths[p][method]['summary'])
                    with open(self.PATH_TO_DIR
                                   + '/'
                                   + self.TITLE_MAIN_DIR
                                   + '/'
                                   + r
                                   + '/'
                                   + result_['operationId']
                                   + "_data_unsuccessful"
                                   + '.json', "w") as file:
                        json.dump(json_data_unsuccessful, file)
        with open("directories.json", "a") as file:
            json.dump(tags, file)






